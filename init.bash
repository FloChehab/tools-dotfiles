#!/usr/bin/env bash

set -o nounset -o pipefail -o errexit

print_title() {
  local line="--------------------------------------------------------------------------------"

  printf "%s%s%s\n" "+-" "${line:0:${#1}}" "-+"
  printf "%s%s%s\n" "| " "${1}" " |"
  printf "%s%s%s\n" "+-" "${line:0:${#1}}" "-+"
}

create_symlinks_auto() {
  local SYMLINK_PATH="${HOME}"

  for file in "${SCRIPT_DIR}/symlinks"/*; do
    local basenameFile="$(basename "${file}")"
    [[ -r ${file} ]] && [[ -e ${file} ]] && rm -f "${SYMLINK_PATH}/.${basenameFile}" && ln -s "${file}" "${SYMLINK_PATH}/.${basenameFile}"
  done
}

create_symlinks_manual() {
  source "${SCRIPT_DIR}/sources/_first"

  mkdir -p ~/.config/git
  mkdir -p ~/.config/broot

  local gitconfig_path=~/.config/git/config
  rm -f ${gitconfig_path}
  ln -s "${SCRIPT_DIR}/symlinks_manual/gitconfig" "${gitconfig_path}"

  local gitignore_path=~/.config/git/ignore
  rm -f ${gitignore_path}
  ln -s "${SCRIPT_DIR}/symlinks_manual/gitignore_global" "${gitignore_path}"

  local broot_conf=~/.config/broot/conf.toml
  rm -f ${broot_conf}
  ln -s "${SCRIPT_DIR}/symlinks_manual/broot.toml" "${broot_conf}"
}

main() {
  local SCRIPT_DIR
  SCRIPT_DIR="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"

  local ARGS="${*}"
  if [[ -z "${ARGS}" ]] || [[ "${ARGS}" =~ symlinks ]]; then
    print_title "symlinks"
    create_symlinks_auto
    create_symlinks_manual
  fi
}

main "${@:-}"

unset -f main
