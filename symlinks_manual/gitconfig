[core]
	editor = vim
	autocrlf = input
	excludesfile = ~/.config/git/ignore
	ignorecase = false
	pager = delta --dark --max-line-distance=0.6
	# pager = delta --dark --plus-color="#012800" --minus-color="#340001" --theme="Monokai Extended"

[interactive]
	diffFilter = delta --color-only

[pull]
	rebase = merges

[push]
	default = current

[fetch]
	prune = true

[commit]
	gpgsign = true

[rebase]
	autosquash = true
	autostash = true

[log]
	abbrevCommit = true

[color]
	ui = true

[merge]
	tool = vscode

[mergetool "vscode"]
	cmd = code --wait $MERGED

[diff]
	mnemonicPrefix = true
	renames = true
	tool = vscode

[difftool "vscode"]
	cmd = code --wait --diff $LOCAL $REMOTE
	trustExitCode = true

[alias]
	main-branch-name = "! git branch -avv | grep -q remotes/origin/master && echo 'master' || echo 'main'"
	current-branch = rev-parse --abbrev-ref HEAD
	current-remote-branch = "! git rev-parse --symbolic-full-name --abbrev-ref @{upstream}"
	fetch-current-branch = "! git fetch $(git current-remote-branch | sed 's!/! !')"
	diff-remote = "! git fetch-current-branch && git diff $(git current-remote-branch)"
	set-upstream = "! git branch --set-upstream-to=origin/$(git current-branch) $(git current-branch)"
	log-pretty = log --color --pretty=format:'%Cred%h%Creset%C(yellow)%d%Creset %s %Cgreen(%cr) %C(bold blue)<%an>%Creset'
	rl = reflog --format='%C(auto)%h %<|(20)%gd %C(blue)%cr%C(reset) %gs (%s)'
	pr = "! [[ \"$(git remote get-url --push $(git remote show | head -1))\" =~ ^.*@(.*):(.*)$ ]] && \"${DEFAULT_OPENER}\" \"https://${BASH_REMATCH[1]}/${BASH_REMATCH[2]%.git}/compare/master...$(git symbolic-ref -q HEAD | sed -e 's|^refs/heads/||')?expand=1\""
	fco = "! git checkout \"$(git branch -vv --all | fzf --height 20 --ansi --reverse -q 'remotes/' | awk '{print $1}' | sed 's|^remotes/origin/||')\""
	rbm = pull --rebase origin $(git main-branch-name)
	tidy = rebase -i origin/$(git main-branch-name)
	commend = commit --amend --no-edit
	pushu = push -u
	pushf = push --force-with-lease
	co = checkout
	d = diff -w
	s = status -sb
	unstage = reset HEAD --
	changelog = ! git log --color --no-merges --pretty=format:'%Cred%h%Creset %s' "${2:-HEAD}...${1}"
	find = log --color --pretty=format:'%Cred%h%Creset -%C(yellow)%d%Creset %s %Cgreen(%cr) %C(bold blue)<%an>%Creset' -S
	filehistory = log --color --graph --pretty=format:'%Cred%h%Creset -%C(yellow)%d%Creset %s %Cgreen(%cr) %C(bold blue)<%an>%Creset' --
	linehistory = log --color --graph --pretty=format:'%Cred%h%Creset -%C(yellow)%d%Creset %s %Cgreen(%cr) %C(bold blue)<%an>%Creset' -L
	sweep = "! git prune && git remote prune origin && git gc && git branch --merged | egrep -v '(^\\*|master)' | xargs git branch -d"
	unshallow = "! git remote set-branches origin '*' && git pull --unshallow"
	tree = log --color --graph --pretty=format:'%Cred%h%Creset -%C(yellow)%d%Creset %s %Cgreen(%cr) %C(bold blue)<%an>%Creset'
	ri = "!f() { git rebase -i \"$1^\"; }; f"
	fix = "!f() { git commit --fixup=\"$1\";}; f"
	ffix = "! git commit --fixup $(git log-pretty $(git main-branch-name)..$(git current-branch) | fzf --height 20 --ansi --reverse | awk '{print $1}')"
	permission-reset = "!git diff -p -R --no-ext-diff --no-color | grep -E '^(diff|(old|new) mode)' --color=never | git apply"

[include]
	path = user
