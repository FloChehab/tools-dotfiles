tools-dotfiles
==============

***My dotfiles.***

*(Inspired by the awesome https://github.com/ViBiOh/dotfiles)*

## Installation

```bash
mkdir -p ~/git
cd ~/git && git clone https://gitlab.com/FloChehab/tools-dotfiles.git
./tools-dotfiles/init.bash symlinks
```

## SSH

```bash
ssh-keygen -t ed25519 -a 100 -C "$(whoami)@$(hostname)" -f ~/.ssh/id_ed25519
```
